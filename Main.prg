Function main
	'Check motion
	init_robot
	Home
	Print "Moving to home position"
	Go CurPos +X(30)
	Go CurPos +Y(30)
	Go CurPos +U(-20)
	
	'Check IO
	Print "Turning on suction."
	On suction
	Print "Suction on. Please confirm. Turning off suction in 5 seconds."
	Wait 5
	Print "Turning off suction."
	Off suction
	
	Print "Turning on dispense."
	On dispense
	Print "Dispense on. Please confirm."
	Wait 1
	Print "Turning off dispense."
	Off dispense
	
	Print "Changing end effector."
	On rotate2
	Wait 1
	Off rotate2
	Print "End effector changed. Please confirm. Reverting in 5 seconds."
	Wait 5
	Print "Reverting end effector."
	On rotate1
	Wait 1
	Off rotate1
	
	Print "Turning on ring light."
	On DF242
	Print "Ring light on. Please confirm."
	Wait 1
	Print "Turning off ring light."
	Off DF242
	
	Print "Turning on spot light."
	On SL164
	Print "Spot light on. Please confirm."
	Wait 1
	Print "Turning off spot light."
	Off SL164
	
	'Check vision
	'Please confirm all cameras are connected and click the Vision Guide icon (the turquoise camera)
	'May need to hook up CV2 vision controller directly to a monitor first to change the IP address
	'Go to setup -> system configuration -> vision and set up all cameras
	'Run vision sequences 	
	
Fend


'********************************* SECTION ONE - ESSENTIALS ******************************************************
'*****************************************************************************************************************

Function init_robot
	Reset 'reset servos and motor settings
	If Motor = Off Then 'energize motors
		Motor On
	EndIf
	Power Low 'low torque
	Speed 100	'speed
	Accel 80, 80 'accel, dcel, as percentages
Fend

Function init_robot_prod
	Reset 'reset servos and motor settings
	If Motor = Off Then 'energize motors
		Motor On
	EndIf
	Power High 'high torque
	Speed 80	'speed
	Accel 60, 60 'accel, decel, as percentages
	'right now speeds and accels settings are left default (ptp movements)
	Fine 300, 300, 300, 300 'lets give this a shot, tuning the positioning accuracy...
	'default values for fine are 1250 across the board
Fend

Function DISP
	Arm 0
	Tool 2 'change end effector
	On rotate1 'signal actuation
	Wait 0.5
	Off rotate1 'turn off signal
Fend

Function SUCT
	Arm 0
	Tool 1 'change end effector
	On rotate2 'signal actuation
	Wait 0.5
	Off rotate2 'turn off signal
Fend

Function CAM
	Arm 3
	Tool 0 'change end effector
	Go CurPos :Z(-25) 'raise Z axis
Fend

Function CAMJ4
	Arm 0
	Tool 3
	Go CurPos :Z(-25) 'raise Z axis
Fend

Function DispenseGlue(ind As Integer)
	LoadPoints "outbox.pts" 'load points to deposit glue (outbox)
	Go CurPos :Z(-25)
	
	Arm 0
	Tool 2 'set end effector
	Go P(ind) :Z(-CZ(P(ind) @0) - 25) :U(0) 'go to XY position of point, Z(-25)
	Go CurPos -Z(25) 'drop end effector to make room for rotation (tubing was intefering)
	DISP

	Go P(ind) :U(0) 'move to dispense location
	Wait 0.2
	On dispense 'dispense
	Wait 1
	Off dispense
	Wait 1
	Speed 20
	Go CurPos :Z(-25) 'move tip out of the way
	SUCT 'reset end effector
	Speed 80
Fend
 
Function Needle_Offset(xChange As Integer, yChange As Integer)
	'moves needle (tool 2) offset specified amount in mm. Remember to orient yourself in the tool coordinate system.
	'signs are flipped (-x/yChange) due to how changing tool offset works. This way, <xChange, yChange> is a vector from current tool position to target tool position.
	
	'Print TLSet(2)
	TLSet 2, TLSet(2) +X(-xChange) +Y(-yChange)
	'Print TLSet(2)
	
Fend

